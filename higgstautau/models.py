"""Parameterized probability distributions for given propositions"""
import numpy
import compile
import rotor
from numpy import (
    array,
    exp, log,
    power, sqrt, cbrt,
    sin, cos, arccos, pi,
    tan, arctan, arctan2,
    cosh, sinh, arccosh,
    minimum,
)
from compile import jit
from rotor import (
    by_trig, by_etpm, rstr, mul,
    complex_conjugate, transform
)

# todo convert to using math and cmath functions where appropriate
# todo validate helicity sign convention against Madgraph
# global constants
HIGGS_MASS = 125.0 # GeV
Z_MASS = 91.18 # GeV
Z_WIDTH = 2.441404 # GeV
TAU_MASS = 1.777 # GeV
BEAM_ENERGY = 6500.0 # GeV
PI = pi
THIRD = 1./3.

# utility
@jit
def copysign(x, y):
    """Return x with the sign of y"""
    return (-x if y < 0. else x)
   
@jit   
def ppow(x, y):
    """Return x**y for x > 0"""
    return exp(y*log(x))

# cauchy distribution functions
@jit
def cauchy_cdf(x):
    """Return the cumulative unit Cauchy distribution"""
    return 0.5 + arctan(x)/PI

@jit
def cauchy_ppf(u):
    """Return the unit Cauchy distribution quantile function"""
    return tan(PI*(u - 0.5))

# tau model
TAU_ENERGY_A = 0.3432408465772238
TAU_ENERGY_b = 0.16215513469457746
TAU_ENERGY_B = 0.5/(1. + TAU_ENERGY_b)
TAU_ENERGY_C = TAU_ENERGY_B*TAU_ENERGY_b
@jit
def tau_energy_fraction(u):
    """Return a mapped energy from the marginal distribution

    Arguments:
    u --- (0,1) parameter
    """
    x = ppow(u,TAU_ENERGY_A)*(TAU_ENERGY_B + TAU_ENERGY_C*u)
    # one Newton's method iteration
    x = (u + x*x*x*(32. - 48.*x))/(x*x*(48. - 64.*x))
    return x

TAU_COS_THETA_CUT = 1e-12
@jit
def tau_cos_theta(sz, x):
    """Return a mapped cos angle within bounds

    Arguments:
    sz --- z component of a point on the unit sphere
    x --- energy fraction
    """
    n = 0.25 - x
    # Solve the quadratic or linear approx when near singular
    if n*n > TAU_COS_THETA_CUT:
        d = 0.75 - x
        return (sqrt(n*n + d*d + 2.*n*d*sz) - d)/n
    else:
        return sz

@jit
def tau_lepton(u, s, hel):
    """Return four-momentum for tau to massless lepton

    Arguments:
    u --- (0,1) param for lepton energy fraction
    s --- (x,y,z) coordinates for a point on the unit sphere
    hel --- discrete helicity sign +-1
    """
    sx, sy, sz = s
    energy_fraction = tau_energy_fraction(u)
    energy = TAU_MASS*energy_fraction
    cos_theta = tau_cos_theta(sz, energy_fraction)
    energy_t = energy*sqrt((1. - cos_theta*cos_theta)/(1. - sz*sz))
    x = energy_t*sx
    y = energy_t*sy
    z = copysign(energy*cos_theta, hel)
    return (energy, x, y, z)

@jit
def tau_jet(u, s, hel):
    """Return four-momentum for tau to jet

    Arguments:
    u --- (0,1) param for tau neutrino energy fraction
    s --- (x,y,z) coordinates for a point on the unit sphere
    hel --- discrete helicity sign +-1
    """
    ve, vx, vy, vz = tau_lepton(u, s, hel)
    return (TAU_MASS - ve, -vx, -vy, -vz)

# higgs model
HIGGS_A = 0.5*arccosh(0.5*HIGGS_MASS/TAU_MASS)
HIGGS_COSH_A = cosh(HIGGS_A)
HIGGS_SINH_A = sinh(HIGGS_A)
@jit
def higgs_rotor(s):
    """Return a rotor from higgs boson rest frame to tau decay product

    Arguments:
    s --- (x,y,z) coordinates for a point on the unit sphere
    """
    sx, sy, sz = s
    t = 0.5*arccos(sz)
    p = 0.5*arctan2(sy, sx)
    ct, st = cos(t), sin(t)
    cp, sp = cos(p), sin(p)
    return by_trig(HIGGS_COSH_A, HIGGS_SINH_A, ct, st, cp, sp)

# gluon higgs pdf
GG_STAR = 0.5*log(2.*BEAM_ENERGY/HIGGS_MASS)
GG_A = 0.21572109995753197
GG_B = 0.8636978411660811
GG_C = 0.829240964142843
GG_D = -0.8702571668598835
GG_E = 0.8042414452562315
@jit
def gg_h_pdf(u):
    """Return rotor approximating a quark parton distribution at higgs mass

    Arguments:
    u --- (0,1) parameter
    """
    
    # flip to lower half
    v = minimum(u, 1. - u)
    # make fit to rapidity / max(rapidity), then scale to half rapidity
    R = ppow(v, GG_A)*(GG_B + v*(GG_C + v*(GG_D + GG_E*v)))
    a = GG_STAR*(R - 1. if (u < 0.5) else 1. - R)
    return (complex(cosh(a)), complex(0.), complex(0.), complex(0.,sinh(a)))

@jit
def pp_h_lj(x):
    """Return four-momenta for lepton and jet in a pp>h>tautau model

    Arguments:
    x --- contains
        u3 --- (0,1)^3 parameters [z boost, lep energy, jet energy]
        sh, sl, sj --- 3 sphere coordinates for [higgs, lep, jet]
        hel --- discrete helicity sign +-1
    """
    u3, sh, sl, sj, hel = x
    # partons to higgs
    R = gg_h_pdf(u3[0])
    # higgs to taus
    H = higgs_rotor(sh)
    # tau to lepton
    lep = tau_lepton(u3[1], sl, hel)
    lep = transform(mul(R, H), lep)
    # other tau to jet
    jet = tau_jet(u3[2], sj, -hel)
    jet = transform(mul(R, complex_conjugate(H)), jet)
    return lep, jet

# scaler z model
@jit
def z_costheta(x):
    """Return costheta for z mapped from x in (-1,1)"""
    t = ppow(sqrt(1. + 4.*x*x) - 2.*x, THIRD)
    return 1./t - t

@jit
def z_rotor(s, mass):
    """Return a rotor from scaler z boson rest frame to tau decay product

    Arguments:
    s --- (x,y,z) coordinates for a point on the unit sphere
    """
    sx, sy, sz = s
    t = 0.5*arccos(z_costheta(sz))
    ct, st = cos(t), sin(t)
    p = 0.5*arctan2(sy, sx)
    cp, sp = cos(p), sin(p)
    za = 0.5*arccosh(0.5*mass/TAU_MASS)
    ca, sa = cosh(za), sinh(za)
    return by_trig(ca, sa, ct, st, cp, sp)

# quark z pdf
QQ_STAR = 0.5*log(2.*BEAM_ENERGY)
QQ_A = 0.26717575375857194
QQ_B = 0.5494565947627952
QQ_C = 1.3811743365851248
QQ_D = -0.239493911899258
QQ_E = 0.496804138598161
QQ_F = -0.6211388355913177
@jit
def qq_z_pdf(u, mass):
    """Return rotor approximating a quark parton distribution at z mass

    Arguments:
    u --- (0,1) parameter
    mass --- z resonance mass
    """
    # flip to lower half
    v = minimum(u, 1. - u)
    # make fit to rapidity / max(rapidity), then scale to half rapidity
    R = ppow(v, QQ_A)*(QQ_B + v*(QQ_C + v*(QQ_D + v*(QQ_E + QQ_F*v))))
    a = GG_STAR*(R - 1. if (u < 0.5) else 1. - R)
    return (complex(cosh(a)), complex(0.), complex(0.), complex(0.,sinh(a)))

ZMASS_C = Z_MASS*Z_WIDTH
ZMASS_D = Z_MASS*Z_MASS
# 15 widths as allowed by MadGraph run card
ZMASS_B = cauchy_cdf(((Z_MASS - 15.*Z_WIDTH)**2 - ZMASS_D)/ZMASS_C)
ZMASS_A = cauchy_cdf(((Z_MASS + 15.*Z_WIDTH)**2 - ZMASS_D)/ZMASS_C) - ZMASS_B
@jit
def z_mass(u):
    """Return resonance mass for z model

    Arguments:
    u --- (0,1) parameter
    """
    Q2 = ZMASS_D + ZMASS_C*cauchy_ppf(ZMASS_B + ZMASS_A*u)
    return sqrt(Q2)

@jit
def pp_z_lj(x):
    """Return four-momenta for lepton and jet in a pp>scaler z>tautau model

    Arguments:
    x --- contains
        u4 --- (0,1)^4 parameters [z mass, z boost, lep energy, jet energy]
        sz, sl, sj --- 3 sphere coordinates for [z, lep, jet]
        hel --- discrete helicity sign +-1
    """
    u4, sh, sl, sj, hel = x
    # choose z mass
    mass = z_mass(u4[0])
    # partons to scaler z
    R = qq_z_pdf(u4[1], mass)
    # scaler z to taus
    Z = z_rotor(sh, mass)
    # tau to lepton
    lep = tau_lepton(u4[2], sl, hel)
    lep = transform(mul(R, Z), lep)
    # other tau to jet
    jet = tau_jet(u4[3], sj, hel)
    jet = transform(mul(R, complex_conjugate(Z)), jet)
    return lep, jet

# todo warmup
# test
if __name__ == "__main__":
    # tau
    print(tau_energy_fraction(0.1))
    print(tau_energy_fraction(0.9))
    print(tau_energy_fraction(1. - 1e-13))
    print(tau_cos_theta(0.5, 0.25))
    print(tau_cos_theta(0.5, 0.26))
    s = (0.1, 0.1, sqrt(1-2*0.01))
    L = tau_lepton(0.1, s, +1)
    J = tau_jet(0.1, s, +1)
    print(L)
    print(J)
    print(L + J)
    # higgs
    tau = (1.777, 0., 0., 0.)
    R = higgs_rotor(s)
    print(rstr(R))
    print(transform(R, tau))
    print(transform(R, L))
    print(transform(R, J))
    R2 = complex_conjugate(R)
    print(rstr(R2))
    print(transform(R2, tau))
    print(transform(R2, L))
    print(transform(R2, J))
    # gg
    higgs = (HIGGS_MASS, 0., 0., 0.)
    print(transform(gg_h_pdf(0.1), higgs))
    print(transform(gg_h_pdf(0.9), higgs))
    print(transform(gg_h_pdf(0.501), higgs))
    # full
    lj = pp_h_lj(((0.1,)*3, s, s, s, +1))
    print(lj[0])
    print(lj[1])
    lj = pp_z_lj(((0.5,) + (0.1,)*3, s, s, s, +1))
    print(lj[0])
    print(lj[1])