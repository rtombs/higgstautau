"""Script for running pseudodata analysis on desktop"""
import sys
import time
import numpy
import pseudodata1
import likelihood
import compile
from time import time
from numpy import exp, sqrt, log, arange
from pseudodata1 import (
    iter_calc,
    make_sampler_lj,
    init_h, init_z,
    step_h, step_z,
    joint_h, joint_z
)
from likelihood import e_pt_eta, delta_phi
from compile import jit

# read in launch instructions todo
assert len(sys.argv) == 4, "Wrong number of intputs. Need type, [start, finish)"
type = str(sys.argv[1])
if type == "h":
    pseudodata = numpy.load("in/pseudodata_h.npy")
elif type == "z":
    pseudodata = numpy.load("in/pseudodata_z.npy")
else:
    raise ValueError("Type must be h or z, received \"" + type + "\".")

start = int(sys.argv[2])
finish = int(sys.argv[3])

# config
nactive = 800
nwalk = 40
rescale = exp(-1/nactive)
Hguess = 25.
sample_h = make_sampler_lj(nactive, nwalk, rescale, Hguess,
                           step_h, init_h, joint_h)
sample_z = make_sampler_lj(nactive, nwalk, rescale, Hguess,
                           step_z, init_z, joint_h)

@jit
def model_compare(data):
    """Compare h and z models on this data"""
    # h model
    logZh, Hh = sample_h(data)
    # z model
    logZz, Hz = sample_z(data)
    return logZh, logZz, Hh, Hz
    
# warmup
model_compare(pseudodata[0])

# time and run the analysis
time_start = time()
results = []
for i, data in enumerate(pseudodata[start:finish]):
    print("\r%d" % (i + 1), end="")
    results.append(model_compare(data))
time_tot = time() - time_start
print("\nTook %.6e seconds (%.6e s per)" %
      (time_tot, time_tot/len(results)))
outname = ("out/results_%s_%d_%d_%d_start%d_finish%d.npy" %
           (type, nactive, nwalk, Hguess, start, finish))
numpy.save(outname, results)
print("Saved as \"" + outname + "\".")
print(" ~fin")
