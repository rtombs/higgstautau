# performance tests for models.py
import numpy
import compile
import models
from numpy import sqrt
from numpy.random import rand, randn, randint
from compile import jit
from models import pp_h_lj, pp_scalerz_lj

@jit
def rand_sphere():
    sx, sy, sz = randn(3)
    norm = 1. / sqrt(sx*sx + sy*sy + sz*sz)
    return (sx*norm, sy*norm, sz*norm)

@jit
def test_pp_h_lj():
    u = rand(3)
    sh = rand_sphere()
    sl = rand_sphere()
    sj = rand_sphere()
    hel = 1 - 2*randint(2)
    pp_h_lj((u, sh, sl, sj, hel))
    

@jit
def test_pp_z_lj():
    u = rand(4)
    sz = rand_sphere()
    sl = rand_sphere()
    sj = rand_sphere()
    hel = 1 - 2*randint(2)
    pp_scalerz_lj((u, sz, sl, sj, hel))


# warmup
test_pp_h_lj()
test_pp_z_lj()

"""
python -m timeit -s"import times;f=times.test_pp_h_lj" "f()"
100000 loops, best of 3: 2.15 usec per loop

python -m timeit -s"import times;f=times.test_pp_z_lj" "f()"
100000 loops, best of 3: 2.25 usec per loop
"""