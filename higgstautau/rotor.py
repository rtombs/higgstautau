"""Lorentz rotors"""
import numpy
import compile
from numpy import (
    array, asarray, inner,
    sqrt, exp, log,
    sin, cos, arccos,
    cosh, sinh, arccosh,
)
from compile import jit
# todo set up
# todo improve doc
# todo look back at older methods of exp, log for normalization, bivectors
# todo cast into language of spacetime algebra
@jit
def sinc(x):
    """Return zero-safe sinc"""
    return (x and sin(x)/x)

"""Rotor is a 4 component complex tuple"""
@jit
def mul(A, B):
    """Return rotor product AB"""
    aw, ax, ay, az = A
    bw, bx, by, bz = B
    # a0b0 - a dot b
    w = aw*bw - ax*bx - ay*by - az*bz
    # a0b + b0a + a cross b
    x = aw*bx + bw*ax - az*by + ay*bz
    y = aw*by + bw*ay - ax*bz + az*bx
    z = aw*bz + bw*az - ay*bx + ax*by
    return (w, x, y, z)

@jit
def complex_conjugate(A):
    """Return complex conjugate of A"""
    w, x, y, z = A
    return (w.conjugate(), x.conjugate(), y.conjugate(), z.conjugate())

@jit
def quaternion_conjugate(A):
    """Return quaternion conjugate of A"""
    w, x, y, z = A
    return (w, -x, -y, -z)

@jit
def conjugate(A):
    """Return composition of complex and quaternion conjugates of A"""
    return quaternion_conjugate(complex_conjugate(A))

@jit
def transform(A, p):
    """Return transformation to four-vector p by rotor a"""
    pw, px, py, pz = p
    p = (complex(pw), complex(0.,px), complex(0.,py), complex(0.,pz))
    qw, qx, qy, qz = mul(mul(A, p), conjugate(A))
    return (qw.real, qx.imag, qy.imag, qz.imag)

# Constructors
@jit
def by_exp(x, y, z):
    """Construct rotor by exponentiation of three complex components"""
    absv = sqrt(x*x + y*y + z*z)
    s = sinc(absv)
    return (cos(absv), s*x, s*y, s*z)

@jit
def by_trig(ca, sa, ct, st, cp, sp):
    """Construct rotor from from trig values"""
    w = complex( ca*ct*cp,-sa*ct*sp)
    x = complex(-ca*st*sp, sa*st*cp)
    y = complex( ca*st*cp, sa*st*sp)
    z = complex( ca*ct*sp, sa*ct*cp)
    return (w, x, y, z)

@jit
def by_angles(a, t, p):
    """Construct rotor from angles

    a = hyperbolic boost angle / 2
    t = theta / 2
    p = phi / 2
    """
    ca, sa = cosh(a), sinh(a)
    ct, st = cos(t), sin(t)
    cp, sp = cos(p), sin(p)
    return by_trig(ca, sa, ct, st, cp, sp)

@jit
def by_etpm(energy, theta, phi, mass):
    """Construct rotor from rest mass to given energy and angles"""
    a = 0.5*arccosh(energy/mass)
    t = 0.5*theta
    p = 0.5*phi
    return by_angles(a, t, p)

@jit
def by_etp0(energy, theta, phi):
    """Construct rotor from z-photon to given energy and angles"""
    ascale = 0.5/sqrt(energy)
    ca = (energy + 1.)*ascale
    sa = (energy - 1.)*ascale
    # get our own trig
    t = 0.5*theta
    p = 0.5*phi
    ct, st = cos(t), sin(t)
    cp, sp = cos(p), sin(p)
    return by_trig(ca, sa, ct, st, cp, sp)

# Display
def rstr(R):
    """Return a readable string representation"""
    w, x, y, z = R
    r = ("rotor w % .2e + % .2ej\n"
         "      x % .2e + % .2ej\n"
         "      y % .2e + % .2ej\n"
         "      z % .2e + % .2ej")
    rw, rx, ry, rz = w.real, x.real, y.real, z.real
    iw, ix, iy, iz = w.imag, x.imag, y.imag, z.imag
    return r % (rw, iw, rx, ix, ry, iy, rz, iz)

def rrepr(R):
    """Return a faithful string representation"""
    w, x, y, z = R
    return "rotor(" + repr([w, x, y, z]) + ")"

# todo warmup
# todo test