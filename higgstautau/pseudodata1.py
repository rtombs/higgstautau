"""analysis with our model"""
import numpy
import models
import likelihood
import nestedsampling
import compile
import gvar
from numpy import (
    exp, sqrt, array, empty,
)
from numpy.random import randint
from models import pp_h_lj, pp_z_lj
from likelihood import logL_electron, logL_jet
from nestedsampling import (
    init_unit, init_sphere,
    step_unit, step_sphere,
    sample, iter_calc
)
from compile import jit
from gvar import gvar

# todo data preprocessing
# todo get rid of gvar

# pseudo data rounded manually
# lepton then jet
data1 = (
    (117.253122, 20.02178597, 10.654131397, -115.03869361),
    (109.686010445, -40.527941682000005, -21.0004177699, -99.734462363),
)
# alternative
data2 = (
    (52.588658632, 34.737063066, 36.288212761, 15.558569371),
    (74.767107731, -36.631566962, -39.6174125286, 51.7457524915),
)

@jit
def init_h(n, data, joint):
    """Return initialization arrays for h"""
    initv = empty(n)
    initx = []
    for i in range(n):
        u3 = (init_unit(), init_unit(), init_unit())
        sh = init_sphere()
        sl = init_sphere()
        sj = init_sphere()
        hel = +1 - 2*(i % 2) # todo generalize
        x = (u3, sh, sl, sj, hel)
        initv[i] = joint(x, data)
        initx.append(x)
    return initv, initx

@jit
def init_z(n, data, joint):
    """Return initialization arrays for z"""
    initv = empty(n)
    initx = []
    for i in range(n):
        u4 = (init_unit(), init_unit(), init_unit(), init_unit())
        sh = init_sphere()
        sl = init_sphere()
        sj = init_sphere()
        hel = +1 - 2*(i % 2) # todo generalize
        x = (u4, sh, sl, sj, hel)
        initv[i] = joint(x, data)
        initx.append(x)
    return initv, initx

@jit
def joint_h(x, data):
    """Compute h model and likelihood"""
    lep, jet = pp_h_lj(x)
    llep = logL_electron(data[0], lep)
    ljet = logL_jet(data[1], jet)
    return llep + ljet

@jit
def joint_z(x, data):
    """Compute scaler z model and likelihood"""
    lep, jet = pp_z_lj(x)
    llep = logL_electron(data[0], lep)
    ljet = logL_jet(data[1], jet)
    return llep + ljet

@jit
def step_h(x, scale, data):
    """Make a pseudorandom step in input coordinates for h"""
    u3, sh, sl, sj, hel = x
    u3 = (
        step_unit(u3[0], scale),
        step_unit(u3[1], scale),
        step_unit(u3[2], scale),
    )
    sh = step_sphere(sh, scale)
    sqscale = sqrt(scale) # todo improve this relationship
    sl = step_sphere(sl, sqscale)
    sj = step_sphere(sj, sqscale)
    # todo try hel?
    x = (u3, sh, sl, sj, hel)
    return (joint_h(x, data), x)

@jit
def step_z(x, scale, data):
    """Make a pseudorandom step in input coordinates for z"""
    u4, sh, sl, sj, hel = x
    u4 = (
        step_unit(u4[0], scale),
        step_unit(u4[1], scale),
        step_unit(u4[2], scale),
        step_unit(u4[3], scale),
    )
    sh = step_sphere(sh, scale)
    sqscale = sqrt(scale)
    sl = step_sphere(sl, sqscale)
    sj = step_sphere(sj, sqscale)
    x = (u4, sh, sl, sj, hel)
    return (joint_z(x, data), x)

# evolution function
def make_sampler_lj(nactive, nwalk, rescale, Hguess, step, init, joint):
    """Return sample function with compiled-in parameters"""
    N = iter_calc(Hguess, nactive)
    @jit
    def evolve(evolve_state, state):
        """Handle random walk for the demo valuation"""
        scale, data = evolve_state
        i, min_value, active_v, active_x = state
        r = randint(len(active_x))
        v = active_v[r]
        x = active_x[r]
        nstep = 0
        # walk
        for i in range(nwalk):
            vdash, xdash = step(x, scale, data)
            if vdash > min_value:
                x = xdash
                v = vdash
                nstep += 1
        # adjust scale
        if 2*nstep < nwalk:
            scale *= rescale
        else:
            scale /= rescale
        scale = max(min(1., scale), 1e-16)
        return ((scale, data), v, x)
    @jit
    def sampler(data):
        """Return function wrapping sample and evolve state"""
        initv, initx = init(nactive, data, joint)
        return sample(N, initv, initx, evolve, (1., data))
    return sampler

# todo warmup
if __name__ == "__main__":
    nactive = 1000
    nwalk = 20
    rescale = exp(-1/nactive)
    Hguess = 25.
    sample_h = make_sampler_lj(nactive, nwalk, rescale, Hguess,
                               step_h, init_h, joint_h)
    sample_z = make_sampler_lj(nactive, nwalk, rescale, Hguess,
                               step_z, init_z, joint_h)
    def run(data):
        """Run a demo nested sampling integration"""
        print(nactive, "active points")
        print(nwalk, "steps")
        print(rescale, "rescaling")
        print(Hguess, "information guess")
        print(iter_calc(Hguess, nactive), "iterations")
        # h
        logZh, Hh = sample_h(data)
        glogZh = gvar(logZh, sqrt(Hh/nactive))
        print("h: logZ = %s, H = %.6e" % (str(glogZh), Hh))
        # z
        logZz, Hz = sample_z(data)
        glogZz = gvar(logZz, sqrt(Hz/nactive))
        print("z: logZ = %s, H = %.6e" % (str(glogZz), Hz))
        # difference
        print("R = %s" % (glogZh - glogZz))
    print("Nested sampling run data 1")
    run(data1)
    print("Nested sampling run data 2")
    run(data2)