"""Probability of data given model parameters"""
import math
import numpy
import compile
from math import erf
from numpy import (
    pi, sqrt, log, exp,
    arccos, arcsinh,
    arctan2,
    sin, cos, cosh, sinh
)
from numpy.random import randn
from compile import jit
PI = pi

# Utilities
@jit
def e_pt_eta(p):
    """Return e, pT, eta for fourmomentum p"""
    pw, px, py, pz = p
    pT = sqrt(px*px + py*py)
    eta = arcsinh(pz/pT)
    return pw, pT, eta

@jit
def delta_phi(p, q):
    """Return phi angle difference between p, q"""
    pw, px, py, pz = p
    qw, qx, qy, qz = q
    return arctan2(px*qy - py*qx, qx*px + qy*py)

# todo tidy up and simplify this
OO_SQRT_2 = 1./sqrt(2.)
@jit
def precision(sdev):
    """Return precision from standard deviation"""
    return OO_SQRT_2/sdev

GAUSS_NORM = -0.5*log(PI)
@jit
def log_gauss(prec, delta):
    """Return log probability under Gaussian distribution"""
    return GAUSS_NORM + log(prec) - prec*prec*delta*delta

# Constants
# denominaotrs, angular precision, and phi bounds normalization
# todo factory these?
# Config
# standard deviation in angular coordinates
LEP_R_SDEV = 0.02
LEP_E_A = 0.3
LEP_E_B = 0.05
LEP_R_PREC = precision(LEP_R_SDEV)
@jit
def logL_electron(d, h):
    """Return probability of lepton {d}ata given {h}ypothesis"""
    de, dpT, deta = e_pt_eta(d)
    he, hpT, heta = e_pt_eta(h)
    e_prec = precision(LEP_E_A*sqrt(he) + LEP_E_B*he)
    r = log_gauss(e_prec, de - he)
    r += log_gauss(LEP_R_PREC, deta - heta)
    r += log_gauss(LEP_R_PREC, delta_phi(d, h))
    return r

JET_R_SDEV = 0.04
JET_E_A = 1.0
JET_E_B = 0.05
JET_R_PREC = precision(JET_R_SDEV)
@jit
def logL_jet(d, h):
    """Return probability of jet {d}ata given {h}ypothesis"""
    de, dpT, deta = e_pt_eta(d)
    he, hpT, heta = e_pt_eta(h)
    e_prec = precision(JET_E_A*sqrt(he) + JET_E_B*he)
    r = log_gauss(e_prec, de - he)
    r += log_gauss(JET_R_PREC, deta - heta)
    r += log_gauss(JET_R_PREC, delta_phi(d, h))
    return r

# Smearing functions to emulate data
@jit
def smear_single(p, a, b, r_sdev):
    """Smear a single fourmomentum"""
    e, pT, eta = e_pt_eta(p)
    phi = arctan2(p[2], p[1])
    e_sdev = a*sqrt(e) + b*e
    # force positive
    e = max(e + randn()*e_sdev, 1e-15)
    eta += randn()*r_sdev
    phi += randn()*r_sdev
    # rebuild assuming massless
    pT = e/cosh(eta)
    px = pT*cos(phi)
    py = pT*sin(phi)
    pz = pT*sinh(eta)
    return (e, px, py, pz)

@jit
def smear(lep, jet):
    """Smear lepton and jet momenta to pseudo data estimates"""
    L = smear_single(lep, LEP_E_A, LEP_E_B, LEP_R_SDEV)
    J = smear_single(jet, JET_E_A, JET_E_B, JET_R_SDEV)
    return (L, J)

# todo warmup
if __name__ == "__main__":
    L = (117.253122, 20.02178597, 10.654131397, -115.03869361)
    J = (109.686010445, -40.527941682000005, -21.0004177699, -99.734462363)
    print(logL_electron(L, L))
    print(logL_electron(L, J))
    print(logL_jet(J, J))
    print(logL_jet(J, L))
    print("lep", L)
    print("jet", J)
    for i in range(5):
        SL, SJ = smear(L, J)
        print("data lep", SL)
        print("data jet", SJ)