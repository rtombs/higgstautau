"""Code for nested sampling and associated tools"""
import math
import numpy
import compile
from numpy import (
    sqrt, exp, expm1, log, log1p,
    int64,
    array, empty, arange,
    dot, maximum, minimum,
)
from numpy.random import (
    randn, rand, randint,
)
from compile import jit

# todo initial large uniform sample

# Nested sampling core algorithm and utilities
@jit
def logaddexp(a, b):
    """Return log(exp(a)+exp(b))"""
    A = maximum(a, b)
    B = minimum(a, b)
    return A + log1p(exp(B - A))

@jit
def logsumexp(a):
    """Return log(sum exp(a))"""
    A = a.max()
    return A + log(exp(a - A).sum())

@jit
def sample(N, activev, activex, evolve, evolve_state):
    """Return Z, H estimates from a nested sampling run

    Arguments:
    N --- number of iterations
    activev --- initial sample valuations
    activex --- initial sample coordinates
    evolve --- function: state -> new sample within constraint
    evolve_state --- parameters for evolve
    """
    logwidth = 1./len(activev)
    logwidth0 = log(-expm1(-logwidth))
    logZ = -1e308
    eta = 0.
    for i in range(N):
        # locate min
        k = activev.argmin()
        logL = activev[k]
        # accumulate
        logweight = logwidth0 - i*logwidth + logL
        logZ = logaddexp(logZ, logweight)
        eta += exp(logweight)*logL
        # replace
        state = (i, logL, activev, activex)
        evolve_state, v, x = evolve(evolve_state, state)
        activev[k] = v
        activex[k] = x
    # tail contribution todo validate / fix
    logtail = log(logwidth) - N*logwidth + activev
    logZ = logaddexp(logZ, logsumexp(logtail))
    eta += (exp(logtail)*activev).sum()
    return logZ, eta*exp(-logZ) - logZ

def iter_calc(H, nactive, sigma=3.5):
    """Return an estimated number of iterations to reach H with nactive"""
    nf = H*nactive
    return int64(nf + sigma*sqrt(nf))

# Quantization to avoid floating point pitfalls
PRECISION = 52
UA = pow(2., PRECISION)
UB = 1./UA
UC = 0.5*UB
@jit
def quantizeU(x):
    """Return x from [0,1) to fixed precision in (0,1)"""
    return UC + UB*int64(x*UA)

SA = 0.5*UA - 0.5
SB = 0.5*UA
SC = 2.*UB
SD = UB - 1.
@jit
def quantizeS(x):
    """Return x from [-1,1] to fixed precision in (-1,1)"""
    return SD + SC*int64(SB + x*SA)

# Exploration
@jit
def step_unit(u, scale):
    """Return u after Gaussian move of scale in [0,1)"""
    u += scale*randn()
    return quantizeU(u % 1.)

@jit
def step_sphere(s, scale):
    """Return s after Gaussian move of scale on the sphere"""
    sx, sy, sz = s
    dx, dy, dz = randn(3)
    sx += scale*dx
    sy += scale*dy
    sz += scale*dz
    a = 1./sqrt(sx*sx + sy*sy + sz*sz)
    return (quantizeS(sx*a), quantizeS(sy*a), quantizeS(sz*a))

@jit
def init_unit():
    """Return a unit uniform coordinates"""
    return quantizeU(rand())

@jit
def init_sphere():
    """Return a uniform sphere coordinate"""
    sx, sy, sz = randn(3)
    a = 1./sqrt(sx*sx + sy*sy + sz*sz)
    return (quantizeS(sx*a), quantizeS(sy*a), quantizeS(sz*a))

if __name__ == "__main__":
    # Simple demonstration
    DEMO_K = 10.**10.
    @jit
    def demo_logf(x):
        """Return a demo valuation"""
        return DEMO_K*log(x)

    DEMO_NACTIVE = 100
    DEMO_NWALK = 20
    DEMO_RESCALE = exp(-3./DEMO_NACTIVE)
    @jit
    def demo_evolve(evolve_state, state):
        """Handle random walk for the demo valuation"""
        scale = evolve_state
        i, min_value, activev, activex = state
        r = randint(len(activex))
        v = activev[r]
        x = activex[r]
        nstep = 0
        # walk
        for i in range(DEMO_NWALK):
            xdash = step_unit(x, scale)
            vdash = demo_logf(xdash)
            if vdash > min_value:
                x = xdash
                v = vdash
                nstep += 1
        # adjust scale
        if 2*nstep < DEMO_NWALK:
            scale *= DEMO_RESCALE
        else:
            scale /= DEMO_RESCALE
        scale = max(min(1., scale), 1e-16)
        return (scale, v, x)

    # prep
    N = iter_calc(35, DEMO_NACTIVE)
    truelogZ = -log(DEMO_K + 1)
    truelogH = log(DEMO_K + 1) - DEMO_K/(DEMO_K + 1.)
    def demo_ns():
        """Run a demo nested sampling integration"""
        initx = array([init_unit() for i in range(DEMO_NACTIVE)])
        initv = array(list(map(demo_logf, initx)))
        logZ, H = sample(N, initv, initx, demo_evolve, 1.)
        return (logZ, H)

    def printdemo():
        logZ, H = demo_ns()
        print("true %.6e+-%.6e, %.6e" % (truelogZ, 0., truelogH))
        print(" got %.6e+-%.6e, %.6e" % (logZ, sqrt(H/DEMO_NACTIVE), H))

    print(iter_calc(20, 20))
    s = (1., 0., 0.)
    print(s, step_sphere(s, 0.1))
    u = 0.99
    print(u, step_unit(u, 0.1))
    print(tuple(init_unit() for i in range(3)))
    print(init_sphere())
    # nested sampling run
    printdemo()
    printdemo()
    printdemo()
    printdemo()