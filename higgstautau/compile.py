"""Juse In Time compilation of this python code"""
import numba
from numba import jit as njit
# todo nuba flag  NUMBA_DISABLE_JIT http://numba.pydata.org/numba-doc/latest/reference/envvars.html?highlight=environment

def jit(*args, **kwargs):
    """Wrap the numba jit with default arguments"""
    kwargs.setdefault("nopython", True)
    return njit(*args, **kwargs)
